import numpy as np 

"""
These are very tailored to Optio. Part of the code need to be isolated so they
can be used in other models. Any reference to 1000000 is an optio specific criteria.
"""


def mbbefd(c, exposure_base, limit):
    errors = []
    if c <= 0:
        errors.append("c")
    if exposure_base <= 0:
        errors.append("exposure base")
    if limit <= 0:
        errors.append("limit")

    if len(errors) == 0:
        b_of_c = np.exp(3.1 - 0.15 * (1 + c) * c)
        g_of_c = np.exp((0.78 + 0.12 * c ) * c)
        a_of_c = (g_of_c - 1) * b_of_c / (1 - g_of_c * b_of_c )

        limit_to_eb = min(1, limit / exposure_base)

        return np.log((a_of_c + b_of_c ** limit_to_eb) / (a_of_c + 1)) / np.log((a_of_c + b_of_c) / (a_of_c +1))
    elif len(errors) == 1:
        return " ".join(errors) + " must be a positive number."
    else:
        return " ,".join(errors) + " must be positive numbers."

def ilf_power(alpha, limit):
    errors = []
    if alpha <= 0:
        errors.append("c")
    if limit <= 0:
        errors.append("limit")
    if len(errors) == 0:
        return (limit / 1000000) ** alpha
    elif len(errors) == 1:
        return " ".join(errors) + " must be a positive number."
    else:
        return " ,".join(errors) + " must be positive numbers."

def ilf_power(primary_alpha, secondary_alpha, inflection_point, limit):
    errors = []
    if primary_alpha <= 0:
        errors.append("primary_alpha")
    if secondary_alpha <= 0:
        errors.append("secondary_alpha")
    if inflection_point <= 0:
        errors.append("inflection_point")

    if not secondary_alpha <= primary_alpha:
        return "Secondary alpha must be less than or equal to primary alpha."

    if len(errors) == 0:
        if limit <= inflection_point:
            return (limit/1000000) ** primary_alpha
        else:
            return (((limit / 1000000) ** secondary_alpha - (inflection_point / 1000000) ** secondary_alpha) /\
                    ((inflection_point / 1000000) ** secondary_alpha) +1 ) * \
                    (inflection_point / 1000000) ** primary_alpha
    elif len(errors) == 1:
        return " ".join(errors) + " must be a positive number."
    else:
        return " ,".join(errors) + " must be positive numbers."
    
def sliding_scale(df, ebv):
    """
    inputs:
            df:     A pandas dataframe that contains at minimum 4 columns. These columns must be
                    in the order ['band_min', 'band_max', 'incremental_rate', 'cumulative_premium'].
                    You may have additional columns on the end and the naming is not important. As long as
                    the first 4 columns data represent the column names listed above.
            ebv:    'Exposure base value' the value we are looking up in the sliding scale.
                    If the scale is for a monetary value the exposure base value neeeds to be in
                    the same currency as the scale. Please exchange to the currency the scale is written
                    in before using this funciton
    outputs: 
            base_premium : (float)
                    A base premium will be calculated for the exposure value given to the scale.

    """
    # Check that the exposure base is greater than 0
    if ebv <= 0:
        raise ValueError("Exposure base value must be a positive number")

    # check the mimnimum viable columns are there and isolate them
    if len(df.columns) < 4:
        raise ValueError("We expect atleast 4 columns ['band_min', 'band_max', 'incremental_rate', 'cumulative_premium']\n"/
                        "Naming is not important but the fist 4 columns of the dataframe should be the equivelants of these column names.")
    df = df.iloc[:,0:4]

    # Check all values in the remaining dataframe are positive numbers
    if np.any(df < 0 ):
        raise ValueError("The first four columns of the dataframe cannot contain negative numbers.")

    # Rename the columns for easy processing inside the funciton
    df.columns = ['band_min', 'band_max', 'incremental_rate', 'cumulative_premium']

    # Make sure bands are in correct order
    df = df.sort_values(by=["band_min"])

    # Find the exposure band by taking the maximum of band minimum that is less than the exposure base value
    try:
        exposure_band = df[df["band_min"] <= ebv ]["band_min"].idxmax()

    # If we are unable to find a band then no band minimums are below the EBV, so we must be in the lowest band.
    except ValueError:
        exposure_band = 0

    # If we are in the lowest band return the exposure base value multiplied by the rate in that band.
    if exposure_band == 0:
        base_premium =  df['incremental_rate'].iloc[exposure_band] * ebv

    # Sliding scale full calculation
    else:
        cumulative_premium_below_band = df["cumulative_premium"].iloc[exposure_band-1]
        max_exposure_below_band = df["band_max"].iloc[exposure_band-1]
        rate_in_band = df["incremental_rate"].iloc[exposure_band]
        base_premium = cumulative_premium_below_band + rate_in_band * ( ebv - max_exposure_below_band)

    return base_premium
